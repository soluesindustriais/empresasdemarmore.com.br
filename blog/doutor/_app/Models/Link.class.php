<?php

/**
 * Link.class.php [ MODEL ]
 * Classe responsável por organizar o SEO do sistema e realizar a navegação.
 * 
 * @copyright (c) year, Rafael da Silva Lima Web Stylus
 */
class Link {
    
    private $File;
    private $Link;
    
    /** DATA */
    private $Local;
    private $Patch;
    private $Tags;
    private $Data;

    /** @var Seo */
    private $Seo;
    
    function __construct() {
        $this->Local = strip_tags(trim(filter_input(INPUT_GET, 'url', FILTER_DEFAULT)));
        $this->Local = ( $this->Local ? $this->Local : 'index' );
        $this->Local = explode('/', $this->Local);
        $this->File = ( isset($this->Local[0]) ? $this->Local[0] : 'index' );
        $this->Link = ( isset($this->Local[1]) ? $this->Local[1] : null );
        $this->Seo = new Seo($this->File, $this->Link);
    }

    /**
     * Método que retorna as Tags de acordo com o link do __construt instanciado.
     * Retorna as tags no método getTags da class Seo.class.php
     * @return string 
     */
    public function getTags() {
        $this->Tags = $this->Seo->getTags();        
        echo $this->Tags;
    }

    /**
     * Método que retorna os Dados de acordo com o link do __construt instanciado.
     * Retorna os Dados no método getData da class Seo.class.php
     * @return array 
     */
    public function getData() {        
        $this->Data = $this->Seo->getData();
        return $this->Data;
    }

    public function getLocal() {
        return $this->Local;
    }

    public function getPatch() {
        $this->setPatch();
        return $this->Patch;
    }

    ########################################
    ############ MÉTODOS PRIVATE ###########
    ########################################

    private function setPatch() {
        if (file_exists(REQUIRE_PATH . DIRECTORY_SEPARATOR . $this->File . '.php')):
            $this->Patch = REQUIRE_PATH . DIRECTORY_SEPARATOR . $this->File . '.php';
        elseif (file_exists(REQUIRE_PATH . DIRECTORY_SEPARATOR . $this->File . DIRECTORY_SEPARATOR . $this->Link . '.php')):
            $this->Patch = REQUIRE_PATH . DIRECTORY_SEPARATOR . $this->File . DIRECTORY_SEPARATOR . $this->Link . '.php';
        else:
            $this->Patch = REQUIRE_PATH . DIRECTORY_SEPARATOR . '404.php';
        endif;
    }

}
