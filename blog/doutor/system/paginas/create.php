<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 2;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <form data-parsley-validate class="form-horizontal form-label-left" method="post">
    <div class="page-title">
      <div class="title_left">
        <h3><i class="fa fa-file-text-o"></i> Cadastro de páginas</h3>
      </div>
      <div class="clearfix"></div>
      <br/>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
          <div class="pull-right">
            <button type="submit" name="Cadastrar" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=paginas/index'"><i class="fa fa-home"></i></button>
          </div>
          <div class="clearfix"></div>
          <br/>
          <?php
          $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
          $post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
          if (isset($post) && isset($post['Cadastrar'])):
            unset($post['Cadastrar']);
            $post['user_empresa'] = $_SESSION['userlogin']['user_empresa'];

            $cadastra = new Paginas();
            $cadastra->ExeCreate($post);
           
            if (!$cadastra->getResult()):
              $erro = $cadastra->getError();
              WSErro($erro[0], $erro[1], null, $erro[2]);
            else:
              $_SESSION['Error'] = $cadastra->getError();
              header('Location: painel.php?exe=paginas/create&get=true');
            endif;
          endif;

          if (isset($get) && $get == true && !isset($post) && isset($_SESSION['Error'])):
            //COLOCAR ALERTA PERSONALIZADOS
            WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
            unset($_SESSION['Error']);
          endif;
          ?>
          <div class="x_panel">
            <div class="x_content">
              <div class="x_title">
                <h2>Preencha os dados abaixo para cadastrar a página.</h2>                           
                <div class="clearfix"></div>                            
              </div>
              <br/>  
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pag_title">Nome <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" id="pag_title" name="pag_title" required="required" class="form-control col-md-7 col-xs-12" placeholder="Digite o nome da categoria ou sessão" value="<?php
                  if (isset($post['pag_title'])): echo $post['pag_title'];
                  endif;
                  ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pag_description">Descrição, até 160 caractéres <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea name="pag_description" 
                            required="required" 
                            class="form-control col-md-7 col-xs-12 j_contChars" 
                            data-parsley-trigger="keyup" 
                            data-parsley-minlength="140" 
                            data-parsley-maxlength="160" 
                            data-parsley-minlength-message="Você deve digitar no mínimo 140 caracteres!" 
                            data-parsley-validation-threshold="139"><?php
                              if (isset($post['pag_description'])): echo $post['pag_description'];
                              endif;
                              ?></textarea>
                  <div class="j_cont"></div> 
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pag_keywords">Keywords</label>
                <div class="col-md-9 col-sm-9 col-xs-12">                                                                
                  <input id="tags_1" type="text" name="pag_keywords" class="tags form-control col-md-7 col-xs-12" value="<?php
                  if (isset($post['pag_keywords'])): echo $post['pag_keywords'];
                  endif;
                  ?>">
                  <div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
                </div>
              </div>

              <div class="form-group">                                
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pag_content">Conteúdo da página 
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <textarea name="pag_content" class="form-control col-md-7 col-xs-12 j_word"><?php
                    if (isset($post['pag_content'])): echo $post['pag_content'];
                    endif;
                    ?></textarea>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pull-right">
      <button type="submit" name="Cadastrar" class="btn btn-primary"><i class="fa fa-save"></i></button>
      <button type="button" class="btn btn-default" onclick="location = 'painel.php?exe=paginas/index'"><i class="fa fa-home"></i></button>
    </div>
    <div class="clearfix"></div>
    <br/>
  </form>
  <div class="clearfix"></div>
</div>
