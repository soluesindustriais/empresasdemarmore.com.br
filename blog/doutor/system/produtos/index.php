<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 1;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>  
  </div>
  <div class="page-title">
    <div class="title col-md-12 col-sm-6 col-xs-12">
      <h3><i class="fa fa-briefcase"></i> Lista de produtos cadastros</h3>
    </div>
    <div class="clearfix"></div>

    <?php
    $get = filter_input(INPUT_GET, 'get', FILTER_VALIDATE_BOOLEAN);
    if (isset($get) && $get == true && isset($_SESSION['Error'])):
      //COLOCAR ALERTA PERSONALIZADOS
      WSErro($_SESSION['Error'][0], $_SESSION['Error'][1], null, $_SESSION['Error'][2]);
    unset($_SESSION['Error']);
  endif;

  $PerPage = filter_input(INPUT_GET, 'perpage', FILTER_VALIDATE_INT);
  $PerPage = (!empty($PerPage) || isset($PerPage) ? $PerPage : 25);

  $Page = filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);

  $Pager = new Pager("painel.php?exe=produtos/index&perpage={$PerPage}&page=");
  $Pager->ExePager($Page, $PerPage);
  $ReadRecursos = new Read;

  $post = filter_input(INPUT_POST, 'busca', FILTER_DEFAULT);
  $search = (!empty($post) ? urldecode(strip_tags(trim($post))) : null);
  ?>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel">          

        <div class="x_title">

          <h2>Utilize o campo de pesquisa para filtrar sua busca.<small>Você também pode visualizar, editar, excluir e alterar status dos produtos.</small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
          </ul>
          <div class="clearfix"></div>
          <br class="clearfix" />

          <!--BLOCO COM CAMPO DE PESQUISA E ITENS POR PÁGINA-->
          <?php include('inc/filter-search.inc.php'); ?>  
          <!--/BLOCO COM CAMPO DE PESQUISA E ITENS POR PÁGINA-->

        </div>

        <?php
        if (!empty($search)):
          $search = strip_tags(trim(urlencode($search)));
          header('Location: ' . BASE . '/painel.php?exe=produtos/index&busca=' . $search);
        endif;

        $s = filter_input(INPUT_GET, 'busca', FILTER_DEFAULT);

        if (!empty($s)):
          $ReadRecursos->ExeRead(TB_PRODUTO, "WHERE prod_status != :st AND user_empresa = :emp AND (prod_title LIKE '%' :s '%') OR (prod_id LIKE '%' :s '%') OR (prod_codigo LIKE '%' :s '%')  OR (prod_content LIKE '%' :s '%') OR (prod_description LIKE '%' :s '%') ORDER BY prod_title ASC LIMIT :limit OFFSET :offset", "st=3&emp=" . $_SESSION['userlogin']['user_empresa'] . "&s={$s}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
        else:
          $ReadRecursos->ExeRead(TB_PRODUTO, "WHERE prod_status != :st AND user_empresa = :emp ORDER BY prod_id DESC LIMIT :limit OFFSET :offset", "st=3&emp=" . $_SESSION['userlogin']['user_empresa'] . "&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
        endif;

        if (!$ReadRecursos->getResult()):
          $Pager->ReturnPage();
          WSErro("Nenhum item foi encontrado em sua pesquisa.", WS_ALERT, null, "MPI Technology");
        else:
          ?>
          <div class="x_content">

            <div class="table-responsive">
              <table class="table table-striped dtr-inline">
                <thead>
                  <tr>
                    <th>Imagem</th>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Status</th>
                    <th>Ações</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($ReadRecursos->getResult() as $dados):
                    extract($dados);
                    ?>
                    <tr class="j_item" id="<?= $prod_id; ?>">
                      <th scope="row"><a href="../<?= Check::CatByParent($cat_parent) . $prod_name; ?>" target="_blank" title="<?= $prod_title; ?>" class="btn border-dark" style="width: 100px; height: 100px; overflow: hidden;"><?= Check::Image("../doutor/uploads/" . $prod_cover, $prod_title, 'img-responsive', 100, 100); ?></a></th>
                      <td><?= $prod_title; ?></td>                       
                      <td><?= Check::Words($prod_description, 30); ?></td>                                                               
                      <td class="j_GetStatusRecursos" rel="<?= $prod_id; ?>"></td>                                                               
                      <td style="width: 150px;">
                        <div class="btn-group btn-group-xs pull-right">                                                
                          <a class="btn btn-dark" href="../<?= Check::CatByParent($cat_parent) . $prod_name; ?>" target="_blank"><i class="fa fa-eye"></i></a>
                          <button type="button" class="btn btn-primary" onclick="location = 'painel.php?exe=produtos/update&id=<?= $prod_id; ?>'"><i class="fa fa-pencil"></i></button>
                          <button type="button" class="btn btn-danger j_remove" rel="<?= $prod_id; ?>" action="RemoveProduto"><i class="fa fa-trash"></i></button>
                          <button type="button" class="btn j_statusRecursos btn-default" rel="<?= $prod_id; ?>" tabindex="getStatusProd" action="StatusProd" value="<?= $prod_status; ?>"><i class="fa fa-ban"></i></button>
                        </div>                                                        
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>

              <div class="clearfix"></div>
              <hr>
              <div class="col-sm-12">
                <div class="btn-toolbar pull-right">
                  <?php
                  if (!empty($s)):
                    $Pager->ExePaginator(TB_PRODUTO, "WHERE prod_status != :st AND user_empresa = :emp AND (prod_title LIKE '%' :s '%') OR (prod_id LIKE '%' :s '%') OR (prod_codigo LIKE '%' :s '%')  OR (prod_content LIKE '%' :s '%') OR (prod_description LIKE '%' :s '%') ORDER BY prod_title ASC LIMIT :limit OFFSET :offset", "st=3&emp=" . $_SESSION['userlogin']['user_empresa'] . "&s={$s}&limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
                  else:
                    $Pager->ExePaginator(TB_PRODUTO);
                  endif;
                  echo $Pager->getPaginator();
                  ?>
                </div>
              </div>
            </div> 
          </div>
        <?php endif; ?>                              
      </div>             
    </div>  
  </div>  
</div>
<div class="clearfix"></div>
</div>
<!-- /page content -->