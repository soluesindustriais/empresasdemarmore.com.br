<?php
session_start();
//Arquivo principal de configurações do sistema
include('doutor/_app/Config.inc.php');
$Read = new Read;
$getURL = trim(strip_tags(filter_input(INPUT_GET, 'url', FILTER_DEFAULT)));
$setURL = (empty($getURL) ? 'index' : $getURL);
$URL = explode('/', $setURL);
$SEO = new Seo($setURL);
//Verifica chave de ativação da newsletter
$active = filter_input(INPUT_GET, 'newsActive', FILTER_DEFAULT);
if (isset($active) && !empty($active)) :
  $erro = null;
  $newsletter = new Newsletter;
  $newsletter->Ativa($active);
  $erro = $newsletter->getError();
  if (!$newsletter->getResult()) :
    WSErro($erro[0], $erro[1], NULL, $erro[2]);
  else :
    WSErro($erro[0], $erro[1], NULL, $erro[2]);
  endif;
endif;
//PEGAR AS PÁGINAS DE CATEGORIAS
$Pages = array();
$Read->FullRead("SELECT pag_name FROM " . TB_PAGINA . " WHERE pag_status = :stats", "stats=2");
if ($Read->getResult()) :
  foreach ($Read->getResult() as $SinglePage) :
    $Pages[] = $SinglePage['pag_name'];
  endforeach;
endif;
$Read->ExeRead(TB_HOME);
if ($Read->getRowCount() >= 1) :
  $home = true;
endif;
if (in_array($URL[0], $Pages) && file_exists('paginas.php') && empty($URL[1])) :
  if (file_exists("page-{$URL[0]}.php")) :
    require "page-{$URL[0]}.php";
  else :
    require 'paginas.php';
  endif;
elseif (isset($home) && file_exists('inc/home-inc.php') && $URL[0] == 'index') :
  require 'inc/home-inc.php';
elseif (file_exists('home.php') && $URL[0] == 'index') :
  require 'home.php';
elseif (in_array($URL[0], $Pages) && file_exists($URL[0] . '.php') && empty($URL[1])) :
  if (file_exists($URL[0] . ".php")) :
    require $URL[0] . ".php";
  else :
    header('location: ' . RAIZ . '/404');
  endif;
elseif (file_exists($URL[0] . '.php')) :
  require $URL[0] . '.php';
else :
  header('location: ' . RAIZ . '/404');
endif;
//Gera o robots.txt
if (!file_exists('robots.txt')) :
  $RAIZ = RAIZ;
  $robots = "User-agent: *
  Disallow: /doutor/
  Disallow: /inc/
  Disallow: /imagens/informacoes/thumb/
  Sitemap: {$RAIZ}/sitemap.xml";
  $open = fopen('robots.txt', "w");
  fwrite($open, str_replace("'", '"', $robots));
  fclose($open);
endif;
//Gera o .htaccess
if (!file_exists('.htaccess')) :
  $RAIZ = RAIZ;
  $HTACCESS = HTACCESS;
  $htaccesswrite = "

#Definindo o idioma padrão

  DefaultLanguage pt-BR

#Ocultando informações do servidor

  ServerSignature Off

#Redirecionando para HTTPS

  RewriteEngine On
  RewriteCond %{HTTPS} !=on
  RewriteRule ^.*$ https://%{SERVER_NAME}%{REQUEST_URI} [R,L]

#Adicionando www

  RewriteCond %{HTTP_HOST} !^www\. [NC]
  RewriteRule ^ https://www.%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
  Options All -Indexes

#Forçando a codificação UTF-8

  AddDefaultCharset utf-8
  AddCharset utf-8 .html .css .js .xml .json .rss

<IfModule mod_rewrite.c>
  #Reescrita de URL

    RewriteCond %{SCRIPT_FILENAME} !-f
    RewriteCond %{SCRIPT_FILENAME} !-d
    RewriteRule ^(.*)$ index.php?url=$1
    

  #Adiciona www. quando não tem www.

    RewriteCond %{HTTP_HOST} ^{$HTACCESS}
    RewriteRule ^ {$RAIZ}%{REQUEST_URI} [L,R=301]

  #redirects

    #redirect 301 /produto {$RAIZ}/index.php?url=produtos

</IfModule>

#Exibindo uma página 404 personalizada

  errordocument 404 {$RAIZ}/404

#Exibindo erro 403 como 404

  errordocument 403 {$RAIZ}/404

#Impedindo a exibição de diretórios

  Options -MultiViews

#Impede de navegar em pastas sem um documento padrão (index) e exibe Erro 403 - permissão negada

  <IfModule mod_autoindex.c>
    Options -Indexes
  </IfModule>

#Forçando a última versão do IE

  <IfModule mod_setenvif.c>
    <IfModule mod_headers.c>
      BrowserMatch MSIE ie
      Header set X-UA-Compatible 'IE=Edge,chrome=1' env=ie
    </IfModule>
  </IfModule>

#Informando proxies sobre alterações de conteúdo

  <IfModule mod_headers.c>
    Header append Vary User-Agent
  </IfModule>

#Permitindo acesso a web fonts de qualquer domínio

  <FilesMatch '.(ttf|otf|eot|woff|font.css)$'>
    <IfModule mod_headers.c>
      Header set Access-Control-Allow-Origin '*'
    </IfModule>
  </FilesMatch>

#Compressão gzip

  <IfModule mod_deflate.c>
    #html, txt, css, js, json, xml, htc:

      #AddOutputFilterByType DEFLATE text/html text/plain text/css application/json

      #AddOutputFilterByType DEFLATE text/javascript application/javascript application/x-javascript

      #AddOutputFilterByType DEFLATE text/xml application/xml text/x-component


    #webfonts e svg:

    <FilesMatch '.(ttf|otf|eot|svg)$' >
      SetOutputFilter DEFLATE
    </FilesMatch>
  </IfModule>

#Fazendo cache de recursos

  <IfModule mod_expires.c>
    Header set Cache-Control 'public'
    ExpiresActive on
    ExpiresDefault 'access plus 1 month'
    ExpiresByType text/cache-manifest 'access plus 0 seconds'
    ExpiresByType text/html 'access plus 0 seconds'

    #Dados

      ExpiresByType text/xml 'access plus 0 seconds'
      ExpiresByType application/xml 'access plus 0 seconds'
      ExpiresByType application/json 'access plus 0 seconds'

    #Feed RSS

      ExpiresByType application/rss+xml 'access plus 1 hour'

    #Favicon (não pode ser renomeado)

      ExpiresByType image/vnd.microsoft.icon 'access plus 1 week'

    #Imagens, vídeo, audio;

      ExpiresByType image/gif 'access plus 1 month'
      ExpiresByType image/png 'access plus 1 month'
      ExpiresByType image/jpg 'access plus 1 month'
      ExpiresByType image/jpeg 'access plus 1 month'
      ExpiresByType video/ogg 'access plus 1 month'
      ExpiresByType audio/ogg 'access plus 1 month'
      ExpiresByType video/mp4 'access plus 1 month'
      ExpiresByType video/webm 'access plus 1 month'

    #Webfonts

      ExpiresByType font/truetype 'access plus 1 month'
      ExpiresByType font/opentype 'access plus 1 month'
      ExpiresByType font/woff 'access plus 1 month'
      ExpiresByType image/svg+xml 'access plus 1 month'
      ExpiresByType application/vnd.ms-fontobject 'access plus 1 month'

    #CSS / jScript

      ExpiresByType text/css 'access plus 1 month'
      ExpiresByType application/javascript 'access plus 1 month'
      ExpiresByType text/javascript 'access plus 1 month'
  </IfModule>
";
  $htaccess = fopen('.htaccess', "w");
  fwrite($htaccess, str_replace("'", '"', $htaccesswrite));
  fclose($htaccess);
endif;
