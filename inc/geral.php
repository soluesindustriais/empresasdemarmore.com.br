<?
$nomeSite			= 'Empresas de Mármore';
$slogan				= 'Dezenas de empresas de pisos e revestimentos com compostos de quartzo';
// $url				= 'https://www.empresasdemarmore.com.br/';
// $url				= 'http://localhost/projetos/teste-base2/';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }

$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-133729972-29';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//Breadcrumbs
$caminho  = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho .= '<a rel="home" itemprop="url" href="'.$url.'" title="Home"><span itemprop="title">';
$caminho .= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> » ';
$caminho .= '<strong><span class="page" itemprop="title" >'.$h1.'</span></strong></div>';

$caminho2	 = '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminho2	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url"><span itemprop="title">';
$caminho2	.= '<i class="fa fa-home" aria-hidden="true"></i> Home</span></a> » ';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminho2	.= '<span itemprop="title"> Produtos </span></a> » ';
$caminho2	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminho2	.= '<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>';
$caminho2	.= '</div></div></div>';

$caminhogranito	 = '<div class="wrapper">';
$caminhogranito	.= '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhogranito	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url">';
$caminhogranito	.= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> » ';
$caminhogranito	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogranito	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhogranito	.= '<span itemprop="title"> Produtos </span></a> » ';
$caminhogranito	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogranito	.= '<a href="'.$url.'granito-categoria" title="Categoria - Granito" class="category" itemprop="url">';
$caminhogranito	.= '<span itemprop="title"> Categoria - Granito </span></a> » ';
$caminhogranito	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhogranito	.= '<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>';
$caminhogranito	.= '</div></div></div></div></div>';

$caminhomarmore	 = '<div class="wrapper">';
$caminhomarmore	.= '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhomarmore	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url">';
$caminhomarmore	.= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> » ';
$caminhomarmore	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomarmore	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhomarmore	.= '<span itemprop="title"> Produtos </span></a> » ';
$caminhomarmore	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomarmore	.= '<a href="'.$url.'marmore-categoria" title="Categoria - Mármore" class="category" itemprop="url">';
$caminhomarmore	.= '<span itemprop="title"> Categoria - Mármore </span></a> » ';
$caminhomarmore	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomarmore	.= '<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>';
$caminhomarmore	.= '</div></div></div></div></div>';

$caminhocimento_queimado	 = '<div class="wrapper">';
$caminhocimento_queimado	.= '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhocimento_queimado	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url">';
$caminhocimento_queimado	.= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> » ';
$caminhocimento_queimado	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhocimento_queimado	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhocimento_queimado	.= '<span itemprop="title"> Produtos </span></a> » ';
$caminhocimento_queimado	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhocimento_queimado	.= '<a href="'.$url.'cimento-queimado-categoria" title="Categoria - Cimento Queimado" class="category" itemprop="url">';
$caminhocimento_queimado	.= '<span itemprop="title"> Categoria - Cimento Queimado </span></a> » ';
$caminhocimento_queimado	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhocimento_queimado	.= '<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>';
$caminhocimento_queimado	.= '</div></div></div></div></div>';

$caminhopia_de_marmore	 = '<div class="wrapper">';
$caminhopia_de_marmore	.= '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhopia_de_marmore	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url">';
$caminhopia_de_marmore	.= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> » ';
$caminhopia_de_marmore	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhopia_de_marmore	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhopia_de_marmore	.= '<span itemprop="title"> Produtos </span></a> » ';
$caminhopia_de_marmore	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhopia_de_marmore	.= '<a href="'.$url.'pia-de-marmore-categoria" title="Categoria - Pia de Mármore" class="category" itemprop="url">';
$caminhopia_de_marmore	.= '<span itemprop="title"> Categoria - Pia de Mármore </span></a> » ';
$caminhopia_de_marmore	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhopia_de_marmore	.= '<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>';
$caminhopia_de_marmore	.= '</div></div></div></div></div>';

$caminhomicrocimento	 = '<div class="wrapper">';
$caminhomicrocimento	.= '<div id="breadcrumb" itemscope itemtype="http://schema.org/Breadcrumb" >';
$caminhomicrocimento	.= '<a rel="home" href="'.$url.'" title="Home" itemprop="url">';
$caminhomicrocimento	.= '<span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i> Home</span></a> » ';
$caminhomicrocimento	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomicrocimento	.= '<a href="'.$url.'produtos" title="Produtos" class="category" itemprop="url">';
$caminhomicrocimento	.= '<span itemprop="title"> Produtos </span></a> » ';
$caminhomicrocimento	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomicrocimento	.= '<a href="'.$url.'microcimento-categoria" title="Categoria - Microcimento" class="category" itemprop="url">';
$caminhomicrocimento	.= '<span itemprop="title"> Categoria - Microcimento </span></a> » ';
$caminhomicrocimento	.= '<div itemprop="child" itemscope itemtype="http://schema.org/Breadcrumb">';
$caminhomicrocimento	.= '<strong><span class="page" itemprop="title" >'.$h1.'</span></strong>';
$caminhomicrocimento	.= '</div></div></div></div></div>';
?>
