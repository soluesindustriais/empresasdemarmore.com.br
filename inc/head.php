<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br"> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<?php include('inc/geral.php'); ?>

	<link rel="preconnect" href="css/normalize.css">
	<!-- <link rel="stylesheet" href="css/fontawesome.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/> -->

	<link rel="preload" href="css/style.css" as="style">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="slick/slick.css" />

	<title><?= mb_strlen($title . " - " . $nomeSite, "UTF-8") > 65 ? $title : $title . " - " . $nomeSite ?></title>
	<link rel="shortcut icon" href="imagens/img-home/favicon.png">
	<base href="<?= $url ?>">
	<meta name="description" content="<?= ucfirst($desc) ?>">
	<meta name="keywords" content="<?= $h1 . "," . $key ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="geo.position" content="<?= $latitude . ";" . $longitude ?>">
	<meta name="geo.placename" content="<?= $cidade . "-" . $UF ?>">
	<meta name="geo.region" content="<?= $UF ?>-BR">
	<meta name="ICBM" content="<?= $latitude . ";" . $longitude ?>">
	<meta name="robots" content="index,follow">
	<meta name="rating" content="General">
	<meta name="revisit-after" content="7 days">
	<link rel="canonical" href="<?= $url . $urlPagina ?>">
	<?php
	if ($author == '') {
		echo '<meta name="author" content="' . $nomeSite . '">';
	} else {
		echo '<link rel="author" href="' . $author . '">';
	}
	?>
	<meta property="og:region" content="Brasil">
	<meta property="og:title" content="<?= $title . " - " . $nomeSite ?>">
	<meta property="og:type" content="article">
	<meta property="og:url" content="<?= $url . $urlPagina ?>">
	<meta property="og:description" content="<?= $desc ?>">
	<meta property="og:site_name" content="<?= $nomeSite ?>">
	<?php if (file_exists($url . $pasta . $urlPagina . "-01.jpg")) { ?>
		<meta property="og:image" content="<?= $url . $pasta . $urlPagina ?>-01.jpg">
	<?php } ?>
	<meta property="fb:admins" content="<?= $idFacebook ?>">

	<script>
		<? include("js/jquery-1.9.0.min.js"); ?>
	</script>
	<script src="<?= $url ?>js/lazysizes.min.js" defer></script>

	<script type="application/ld+json">
		{
			"@context": "https://schema.org",
			"@type": "Organization",
			"name": "Empresas de marmore",
			"alternateName": "Empresas de marmore - Soluções industriais",
			"url": "https://www.empresasdemarmore.com.br",
			"logo": "https://www.empresasdemarmore.com.br/imagens/img-home/logo.png",
			"sameAs": [
				"https://www.facebook.com/plataformasolucoesindustriais",
				"https://www.instagram.com/solucoesindustriaisoficial/",
				"https://br.linkedin.com/company/solucoesindustriais",
				"https://br.pinterest.com/solucoesindustriaismarketplace/"
			]
		}
	</script>

	<?php include 'inc/fancy.php'; ?>