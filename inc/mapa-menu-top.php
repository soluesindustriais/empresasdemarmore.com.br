<li><a href="<?=$url?>" title="Página Inicial"> Início</a></li>
<li><a href="sobre-nos" title="Sobre Nós <?=$nomeSite?>"> Sobre Nós</a></li>
<li class="dropdown"><a href="produtos" title="Produtos <?=$nomeSite?>"> Produtos</a>
  <ul class="sub-menu">
    <li><a href="granito-categoria" title="Granito <?=$nomeSite?>"> Granito</a>
      <ul class="sub-menu">
        <? include('inc/granito/granito-sub-menu.php');?>
      </ul>
    </li>
    <li><a href="marmore-categoria" title="Mármore <?=$nomeSite?>"> Mármore</a>
      <ul class="sub-menu">
        <? include('inc/marmore/marmore-sub-menu.php');?>
      </ul>
    </li>
    <li><a href="pia-de-marmore-categoria" title="Pia de Mármore <?=$nomeSite?>"> Pia de Mármore</a>
      <ul class="sub-menu">
        <? include('inc/pia-de-marmore/pia-de-marmore-sub-menu.php');?>
      </ul>
    </li>
    <li><a href="cimento-queimado-categoria" title="Cimento Queimado <?=$nomeSite?>"> Cimento Queimado</a>
      <ul class="sub-menu">
        <? include('inc/cimento-queimado/cimento-queimado-sub-menu.php');?>
      </ul>
    </li>
    <li><a href="microcimento-categoria" title="Microcimento <?=$nomeSite?>"> Microcimento</a>
      <ul class="sub-menu">
        <? include('inc/microcimento/microcimento-sub-menu.php');?>
      </ul>
    </li>
  </ul>
</li>
