<div class="logo-top">
  <a href="<?=$url?>" title="início">
  <img src="imagens/img-home/logo.png" alt="Logo - Empresas de Marmore" title="Logo - Empresas de Marmore"></a>
</div>
<ul>
  <li><a href="<?=$url?>" title="Página Inicial">Início</a></li>
  <li><a href="<?=$url?>sobre-nos" title="Sobre Nós <?=$nomeSite?>"> Sobre Nós</a></li>
  <li class="dropdown"><a href="<?=$url?>produtos" title="Produtos">Produtos</a>
    <ul class="sub-menu">
      <? include('inc/sub-menu.php');?>
    </ul>
  </li>
    <li><a href="<?=$url?>blog" title="Blog <?=$nomeSite?>"> Blog</a></li>
    <li><a href="https://faca-parte.solucoesindustriais.com.br/" target="_blank" title="Faça parte"> Faça parte</a></li>
</ul>