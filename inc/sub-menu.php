<li class="dropdown-2">
	<a href="granito-categoria" title="Granito">Granito</a>
	<ul class="sub-menu2">
		<li>Granito</li>
		<? include('inc/granito/granito-sub-menu.php');?>
	</ul>
</li>
<li class="dropdown-2">
	<a href="marmore-categoria" title="Mármore">Mármore</a>
	<ul class="sub-menu2">
		<li>Mármore</li>
		<? include('inc/marmore/marmore-sub-menu.php');?>
	</ul>
</li>
<li class="dropdown-2">
	<a href="pia-de-marmore-categoria" title="Pia de Mármore">Pia de Mármore</a>
	<ul class="sub-menu2">
		<li>Pia de Mármore</li>
		<? include('inc/pia-de-marmore/pia-de-marmore-sub-menu.php');?>
	</ul>
</li>
<li class="dropdown-2">
	<a href="cimento-queimado-categoria" title="Cimento Queimado">Cimento Queimado</a>
	<ul class="sub-menu2">
		<li>Cimento Queimado</li>
		<? include('inc/cimento-queimado/cimento-queimado-sub-menu.php');?>
	</ul>
</li>
<li class="dropdown-2">
	<a href="microcimento-categoria" title="Microcimento">Microcimento</a>
	<ul class="sub-menu2">
		<li>Microcimento</li>
		<? include('inc/microcimento/microcimento-sub-menu.php');?>
	</ul>
</li>
