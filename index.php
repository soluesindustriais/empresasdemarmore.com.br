﻿<?
  $h1 = 'Empresas de Mármore';
  $title = 'Página Inicial';
  $desc = 'Se procura por pisos e revestimentos com compostos de quartzo, você encontra nos resultados do Soluções Industriais, receba diversos orçamentos.';
  include('inc/head.php');
?>
</head>
<body>
  <? include('inc/topo.php'); ?>
  <!--SECTION SLIDER-->
  <section class="cd-hero">
    <div class="title-main"><h1>Empresas de Mármore</h1></div>
    <ul class="cd-hero-slider autoplay">
      <li class="selected">
        <div class="cd-full-width">
          <h2>Pedra de Mármore</h2>
          <p>O mármore é um dos tipos de rochas ornamentais mais utilizados em projetos de construção e decoração dos mais variados estilos. A pedra é versátil e pode ser empregada em pisos, escadas, bancadas e até mesmo em artigos decorativos.</p>
          <a href="<?= $url; ?>pedra-de-marmore" class="cd-btn">Saiba mais</a>
        </div>
      </li>
      <li>
        <div class="cd-full-width">
          <h2>Pia de Mármore</h2>
          <p>Investir em uma pia de mármore é aliar sofisticação com praticidade. Veja a seguir algumas das diversas vantagens de ter uma pia de mármore...</p>
          <a href="<?= $url; ?>pia-de-marmore" class="cd-btn">Saiba mais</a>
        </div>
      </li>
      <li>
        <div class="cd-full-width">
          <h2>Mármore Branco</h2>
          <p>Um dos tipos de pedras ornamentais que podem trazer maior charme e elegância para qualquer tipo de ambiente é o mármore branco. Existem muitos tipos de mármores brancos no mercado, cada um com suas particularidades, mas todos oferecendo um ótimo acabamento para esta pedra.</p>
          <a href="<?= $url; ?>marmore-branco" class="cd-btn">Saiba mais</a>
        </div>
      </li>
    </ul>
    <div class="cd-slider-nav">
      <nav>
        <span class="cd-marker item-1"></span>
        <ul>
          <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
          <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
          <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        </ul>
      </nav>
    </div>
  </section>
  <main>
    <section class="wrapper-main">
      <!--DIV 2 COLS-->
      <div class="main-center">
        <div class="quadro-2 ">
          <h2>Compostos de Quartzo</h2>
          <div class="div-img">
            <p data-anime="left-0">Uma das grandes utilizações do quartzo se dá em bancadas. Isso se deve ao fato de o quartzo não ser um material poroso e, desse modo, sua textura não permite o surgimento de bactérias, além de ser muito resistente a manchas. Assim é possível proteger as pessoas que moram no estabelecimento, facilitando também a limpeza do ambiente. Por esse motivo, o quartzo é um material que raramente precisará de impermeabilizantes.</p>
          </div>
          <div class="gerador-svg" data-anime="in">
            <img src="imagens/img-home/compostos-de-quartzo.jpg" alt="Compostos de Quartzo" title="Compostos de Quartzo">
          </div>
        </div>
        <div class="incomplete-box">
          <ul data-anime="in">
            <li><p>O quartzo é um mineral muito conhecido porque possui muita beleza. Vale ressaltar também que o quartzo é a segunda pedra que se encontra com mais abundância na natureza. Existem variedades de quartzo, e em alguns casos são considerados pedras preciosas.</p></li>
            <li><i class="fas fa-angle-right"></i>Mármore</li>
            <li><i class="fas fa-angle-right"></i>Granito</li>
            <li><i class="fas fa-angle-right"></i>Silestone</li>
            <li><i class="fas fa-angle-right"></i>Milestone</li>
            <li><i class="fas fa-angle-right"></i>Ônix</li>
            <li><i class="fas fa-angle-right"></i>Quartzo</li>
          </ul>
          <a href="<?= $url; ?>granito-e-marmore" class="btn-4">Orçamento Grátis</a>
        </div>
      </div>
      <!--SECTION 3 ICONS-->
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="<?=$url?>imagens/portal/bank.png" class="fas fa-dollar-sign fa-7x" alt="Icone personalizado de um banco">
            <div><p>Preço</p></div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="<?=$url?>imagens/portal/bag.png" class="fas fa-ruler fa-7x" alt="Icone personalizado de uma BAG">
            <div><p>Medida</p></div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <img src="<?=$url?>imagens/portal/delivery.png" class="fas fa-truck-loading fa-7x" alt="Icone personalizado de uma bag de entrega">
            <div><p>Entrega</p></div>
          </div>
        </div>
      </div>
    </section>
    <!--SECTION 3 BANNERS-->
    <section class="wrapper-img">
      <div class="txtcenter"><h2>Produtos <b>Relacionados</b></h2></div>
      <div class="content-icons">
        <div class="produtos-relacionados-1">
          <figure>
            <a href="<?= $url; ?>bancada-de-marmore-para-banheiro">
              <div class="fig-img">
                <h2>Bancada de mármore para banheiro</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-2">
          <figure class="figure2">
            <a href="<?= $url; ?>balcao-de-marmore-para-cozinha">
              <div class="fig-img2">
                <h2>Balcão de mármore para cozinha</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
        <div class="produtos-relacionados-3">
          <figure>
            <a href="<?= $url; ?>lavatorio-de-banheiro-de-marmore">
              <div class="fig-img">
                <h2>Lavatório de banheiro de mármore</h2>
                <div class="btn-5" data-anime="up"> Saiba Mais </div>
              </div>
            </a>
          </figure>
        </div>
      </div>
    </section>
    <!--SECTION GALLERY-->
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li>
              <a href="imagens/img-home/empresas-de-marmore-1.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-1.jpg" title="Empresas de Mármore" alt="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-2.jpg" class="lightbox"  title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-2.jpg" alt="Empresas de Mármore" title="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-3.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-3.jpg" alt="Empresas de Mármore" title="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-4.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-4.jpg" alt="Empresas de Mármore" title="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-5.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-5.jpg" alt="Empresas de Mármore"  title="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-6.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-6.jpg" alt="Empresas de Mármore" title="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-7.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-7.jpg" alt="Empresas de Mármore" title="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-8.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-8.jpg" alt="Empresas de Mármore" title="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-9.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-9.jpg" alt="Empresas de Mármore" title="Empresas de Mármore">
              </a>
            </li>
            <li>
              <a href="imagens/img-home/empresas-de-marmore-10.jpg" class="lightbox" title="Empresas de Mármore">
                <img src="imagens/img-home/thumbs/empresas-de-marmore-10.jpg" alt="Empresas de Mármore" title="Empresas de Mármore">
              </a>
            </li>
          </ul>
        </div>
      </div>
    </section>
  </main>
  <? include('inc/footer.php'); include('inc/fancy.php'); ?>
  <script src="hero/js/modernizr.js"></script>
  <script src="hero/js/main.js"></script>
</body>
</html>
