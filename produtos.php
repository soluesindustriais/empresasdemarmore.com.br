<?
	$h1 = "Produtos";
	$title = "Produtos";
	$desc = "Informações sobre os produtos e serviços comercializados pela empresa. Clique aqui para saber mais detalhes. Dúvidas, entre em contato conosco";
	$var = "Produtos";
	include('inc/head.php');
?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
 	<main>
 		<div class="content">
 			<?= $caminho; ?>
			<h1><?= $h1; ?></h1>
			<article class="full">
				<p>Encontre diversos fornecedores de pisos e revestimentos com compostos de quartzo, cote agora mesmo!</p>
				<ul class="thumbnails-main">
					<li>
						<a rel="nofollow" href="granito-categoria" title="Categoria - Granito">
							<img src="imagens/img-home/thumbs/granito.jpg" alt="Granito" title="Granito">
						</a>
						<h2><a href="granito-categoria" title="Categoria - Granito">Granito</a></h2>
					</li>
					<li>
						<a rel="nofollow" href="marmore-categoria" title="Categoria - Mármore">
							<img src="imagens/img-home/thumbs/marmore.jpg" title="Mármore" alt="Mármore">
						</a>
						<h2><a href="marmore-categoria" title="Categoria - Mármore">Mármore</a></h2>
					</li>
					<li>
						<a rel="nofollow" href="pia-de-marmore-categoria" title="Categoria - Pia de Mármore">
							<img src="imagens/img-home/thumbs/pia-de-marmore.jpg" alt="Pia de Mármore" title="Pia de Mármore">
						</a>
						<h2><a href="pia-de-marmore-categoria" title="Categoria - Pia de Mármore">Pia de Mármore</a></h2>
					</li>
					<li>
						<a rel="nofollow" href="cimento-queimado-categoria" title="Categoria - Cimento Queimado">
							<img src="imagens/portal/thumbs/cimento-queimado.jpg" alt="Cimento Queimado" title="Cimento Queimado">
						</a>
						<h2><a href="cimento-queimado-categoria" title="Categoria - Cimento Queimado">Cimento Queimado</a></h2>
					</li>
					<li>
						<a rel="nofollow" href="microcimento-categoria" title="Categoria - Microcimento">
							<img src="imagens/img-home/thumbs/microcimento.jpg" alt="Microcimento" title="Microcimento">
						</a>
						<h2><a href="microcimento-categoria" title="Categoria - Microcimento">Microcimento</a></h2>
					</li>
				</ul>
			</article>
 		</div>
 	</main>
 </div>
 <? include('inc/footer.php');?>
</body>
</html>
